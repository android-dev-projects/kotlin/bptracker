<img alt="App Splash Screen" src="https://gitlab.com/android-dev-projects/kotlin/bptracker/-/blob/master/screenshots/Picture1.png" />

The BPTracker Application; which is a course project for Advanced Mobile Development course, targets those who have heart conditions and especially blood pressure conditions, where there are disturbances in their blood pressure at different times during the day, where a technique that you measure and record your bp ratings 3 times (fixed times) daily for about 4-7 days (mostly) to know the state of the bp rates and to prescribe the appropriate medications (by the doctors).

The first-time usage creates a user profile with his name, normal bp readings (high and low) and in case of emergencies his blood type, and an emergency contact.

The readings are added as a list, can be create, viewed, updated, and deleted (one by one or all).
 
List of Features
The following features were used in the development process:
•	AndroidX
•	Navigation Component
•	SafeArgs
•	Snackbar
•	WorkManager
•	ListAdapter
•	MVVM Architecture
•	Room
•	Android Lifecycle
•	ViewModel
•	ViewModelFactory
•	Observers
•	DataBinding
•	Material Design
•	TextField
•	Dialogs
•	Notifications
•	Along with basic Android Components
•	RecyclerView, Fragments, etc
