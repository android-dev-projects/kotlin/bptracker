package com.monady.bptracker.adapters

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.monady.bptracker.models.domain.BpReading
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

@BindingAdapter("BpValueText")
fun TextView.text(reading: Int){
    reading?.let {
        this.text = it.toString()
    }
}

@BindingAdapter("bpListItems")
fun RecyclerView.listItems(data: List<BpReading>?){
    val adapter = this.adapter as BpReadingsAdapter
    data?.let{
        adapter.submitList(it)
    }
}

@BindingAdapter("isViewEditable")
fun TextInputEditText.isEditable(boolean: Boolean){
    boolean?.let {
        isClickable = boolean
        isCursorVisible = boolean
        isFocusable = boolean
        isFocusableInTouchMode = boolean
    }
}

@BindingAdapter("setTimestamp")
fun TextView.timestamp(timestamp: Long){
    timestamp?.let{
        //var sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        //val resultDate = Date(timestamp)
        //this.text = sdf.format(resultDate)
        this.text = DateFormat.getDateTimeInstance().format(timestamp)
    }
}