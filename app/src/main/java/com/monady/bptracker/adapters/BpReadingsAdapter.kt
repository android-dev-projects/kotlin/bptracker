package com.monady.bptracker.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.monady.bptracker.databinding.CardBpReadingBinding
import com.monady.bptracker.listeners.BpReadingListener
import com.monady.bptracker.models.domain.BpReading
import com.monady.bptracker.models.domain.toViewModel
import com.monady.bptracker.utils.BpReadingsDiffUtil

class BpReadingsAdapter(private val readingListener: BpReadingListener): ListAdapter<BpReading, BpReadingsAdapter.BpReadingsViewHolder>(BpReadingsDiffUtil()){
    class BpReadingsViewHolder(private val binding: CardBpReadingBinding): RecyclerView.ViewHolder(binding.root){
        companion object{
            fun from(parent: ViewGroup): BpReadingsViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CardBpReadingBinding.inflate(layoutInflater, parent, false)
                return BpReadingsViewHolder(binding)
            }
        }

        fun bind(bpReading: BpReading, readingListener: BpReadingListener){
            binding.bpReading = bpReading.toViewModel()
            binding.readingClickListener = readingListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BpReadingsViewHolder {
        return BpReadingsViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: BpReadingsViewHolder, position: Int) {
        holder.bind(getItem(position), readingListener)
    }
}
