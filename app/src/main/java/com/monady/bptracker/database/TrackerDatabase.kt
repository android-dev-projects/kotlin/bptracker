package com.monady.bptracker.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.monady.bptracker.database.dao.BpReadingDao
import com.monady.bptracker.database.dao.UserProfileDao
import com.monady.bptracker.models.domain.BpReading
import com.monady.bptracker.models.domain.UserProfile

@Database(entities = [BpReading::class, UserProfile::class], version = 1)
abstract class TrackerDatabase: RoomDatabase() {

    abstract val bpReadingDAO: BpReadingDao
    abstract val userProfileDAO: UserProfileDao

    companion object{
        private lateinit var INSTANCE: TrackerDatabase

        fun getINSTANCE(context: Context): TrackerDatabase{
            synchronized(this){
                if(!::INSTANCE.isInitialized){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, TrackerDatabase::class.java, "tracker_database").build()
                    return INSTANCE
                }
            }
            return INSTANCE
        }
    }
}