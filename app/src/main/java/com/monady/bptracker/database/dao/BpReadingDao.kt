package com.monady.bptracker.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.monady.bptracker.models.domain.BpReading

@Dao
interface BpReadingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBpReading(reading: BpReading)

    @Query("select * from bp_readings_table where id = :readingId")
    fun getBpReading(readingId: Int): BpReading

    @Query("select * from bp_readings_table order by updated_at desc")
    fun getAllReadings(): LiveData<List<BpReading>>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateBpReading(reading: BpReading)

    @Query("delete from bp_readings_table where id = :readingId")
    fun deleteBpReading(readingId: Int)

    @Query("delete from bp_readings_table")
    fun deleteAllReadings()
}