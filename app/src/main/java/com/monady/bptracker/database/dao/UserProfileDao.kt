package com.monady.bptracker.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.monady.bptracker.models.domain.UserProfile

@Dao
interface UserProfileDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserProfile(userProfile: UserProfile)

    @Query("select * from user_profile_table limit 1")
    fun getUserProfile(): LiveData<UserProfile>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUserProfile(userProfile: UserProfile)
}