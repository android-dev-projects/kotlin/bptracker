package com.monady.bptracker.listeners

class BpReadingListener(private val clickListener: (bpReadingId: Int) -> Unit) {
    fun onClick(bpReadingId: Int) = clickListener(bpReadingId)
}