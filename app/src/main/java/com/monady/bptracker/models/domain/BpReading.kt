package com.monady.bptracker.models.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.monady.bptracker.models.view.BpReadingView

@Entity(tableName = "bp_readings_table")
data class BpReading(
        @PrimaryKey(autoGenerate = true)
        var id: Int,
        var created_at: Long,
        var updated_at: Long,
        var high: Int,
        var low: Int,
        var indicatorImage: Int
)

fun BpReading.toViewModel(): BpReadingView{
        return BpReadingView(
                id = id,
                created_at = created_at,
                updated_at = updated_at,
                high = high.toString(),
                low = low.toString(),
                indicatorImage = indicatorImage
        )
}