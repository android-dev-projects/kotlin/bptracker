package com.monady.bptracker.models.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.monady.bptracker.models.view.UserProfileView

@Entity(tableName = "user_profile_table")
data class UserProfile(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        var created_at: Long,
        var firstName: String,
        var lastName: String,
        var bloodType: String,
        var normalHighReading: Int,
        var normalLowReading: Int,
        var emergencyContactName: String,
        var emergencyContactNumber: String
)

fun UserProfile.toViewModel(): UserProfileView {
    return UserProfileView(
            id = id,
            created_at = created_at,
            firstName = firstName,
            lastName = lastName,
            bloodType = bloodType,
            normalHighReading = normalHighReading.toString(),
            normalLowReading = normalLowReading.toString(),
            emergencyContactName = emergencyContactName,
            emergencyContactNumber = emergencyContactNumber
    )
}
