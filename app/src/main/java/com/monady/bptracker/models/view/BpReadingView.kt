package com.monady.bptracker.models.view

import com.monady.bptracker.models.domain.BpReading

class BpReadingView (
        var id: Int,
        var created_at: Long,
        var updated_at: Long,
        var high: String,
        var low: String,
        var indicatorImage: Int
)

fun BpReadingView.toDomainModel(): BpReading {
    return BpReading(
            id = id,
            created_at = created_at,
            updated_at = updated_at,
            high = high.toInt(),
            low = low.toInt(),
            indicatorImage = indicatorImage
    )
}