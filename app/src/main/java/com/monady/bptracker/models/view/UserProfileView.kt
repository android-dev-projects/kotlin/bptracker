package com.monady.bptracker.models.view

import com.monady.bptracker.models.domain.UserProfile

data class UserProfileView(
        val id: Int,
        var created_at: Long,
        var firstName: String,
        var lastName: String,
        var bloodType: String,
        var normalHighReading: String,
        var normalLowReading: String,
        var emergencyContactName: String,
        var emergencyContactNumber: String
)

fun UserProfileView.toDomainModel(): UserProfile {
    return UserProfile(
            id = id,
            created_at = created_at,
            firstName = firstName,
            lastName = lastName,
            bloodType = bloodType,
            normalHighReading = normalHighReading.toInt(),
            normalLowReading = normalLowReading.toInt(),
            emergencyContactName = emergencyContactName,
            emergencyContactNumber = emergencyContactNumber
    )
}
