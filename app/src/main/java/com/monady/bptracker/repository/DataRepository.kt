package com.monady.bptracker.repository

import android.app.Application
import com.monady.bptracker.database.TrackerDatabase
import com.monady.bptracker.models.*
import com.monady.bptracker.models.domain.BpReading
import com.monady.bptracker.models.domain.UserProfile
import com.monady.bptracker.models.domain.toViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DataRepository(app: Application) {

    companion object{
        private lateinit var repoInstance: DataRepository

        fun getDataRepository(app: Application): DataRepository{
            synchronized(this){
                if(!::repoInstance.isInitialized){
                    repoInstance = DataRepository(app)
                }
                return repoInstance
            }
        }
    }


    private val trackerDao = TrackerDatabase.getINSTANCE(app.applicationContext).bpReadingDAO
    private val userProfileDao = TrackerDatabase.getINSTANCE(app.applicationContext).userProfileDAO

    fun getAllReadings() = trackerDao.getAllReadings()

    fun getReadingById(readingId: Int) = trackerDao.getBpReading(readingId).toViewModel()

    suspend fun insertReading(reading: BpReading){
        withContext(Dispatchers.IO){
            trackerDao.insertBpReading(reading)
        }
    }

    suspend fun deleteReadingById(readingId: Int){
        withContext(Dispatchers.IO){
            trackerDao.deleteBpReading(readingId)
        }
    }

    suspend fun updateReading(reading: BpReading){
        withContext(Dispatchers.IO){
            trackerDao.updateBpReading(reading)
        }
    }

    suspend fun deleteAllReadings(){
        withContext(Dispatchers.IO){
            trackerDao.deleteAllReadings()
        }
    }

    fun getUserProfile() = userProfileDao.getUserProfile()

    suspend fun insertUserProfile(userProfile: UserProfile){
        withContext(Dispatchers.IO){
            userProfileDao.insertUserProfile(userProfile)
        }
    }

    suspend fun updateUserProfile(userProfile: UserProfile){
        withContext(Dispatchers.IO){
            userProfileDao.updateUserProfile(userProfile)
        }
    }

}