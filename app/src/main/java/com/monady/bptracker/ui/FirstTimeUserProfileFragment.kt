package com.monady.bptracker.ui

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.monady.bptracker.R
import com.monady.bptracker.databinding.FragmentFirstTimeUserProfileBinding
import com.monady.bptracker.models.view.UserProfileView
import com.monady.bptracker.viewmodel.BpViewModel
import com.monady.bptracker.viewmodel.UserProfileViewModel
import com.monady.bptracker.viewmodel.ViewModelFactory
import com.monady.bptracker.work.FireNotificationWorker
import java.util.concurrent.TimeUnit

class FirstTimeUserProfileFragment : Fragment() {
    lateinit var binding: FragmentFirstTimeUserProfileBinding
    lateinit var viewModel: UserProfileViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (requireActivity() as AppCompatActivity).supportActionBar?.show()

        binding = FragmentFirstTimeUserProfileBinding.inflate(layoutInflater)

        welcomeDialog()

        val bloodTypes = listOf("A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-")
        val adapter = ArrayAdapter(requireContext(), R.layout.list_blood_type_item, bloodTypes)
        (binding.userProfileInput.bloodTypeDdl as AutoCompleteTextView)?.setAdapter(adapter)

        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        viewModel = ViewModelProvider(this, viewModelFactory).get(UserProfileViewModel::class.java)

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        binding.userProfileView = UserProfileView(0, 0L, firstName = "", lastName = "", bloodType = "",
                normalHighReading = "", normalLowReading = "",emergencyContactName = "", emergencyContactNumber = "")

        viewModel.addUserProfile.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it){
                    val userProfile = binding.userProfileView!!
                    if(userProfile.firstName == "" || userProfile.lastName == "" || userProfile.bloodType == "" ||
                            userProfile.normalHighReading == "" || userProfile.normalLowReading == "" ||
                            userProfile.emergencyContactName == "" || userProfile.emergencyContactNumber == ""){
                        Snackbar.make(
                                binding.root,
                                "One or more fields are set to empty!",
                                Snackbar.LENGTH_LONG
                        ).show()
                        viewModel.terminateAddUserProfile()
                    }
                    else{
                        userProfile.created_at = System.currentTimeMillis()
                        viewModel.insertUserProfile(userProfile)
                        viewModel.terminateAddUserProfile()
                        confirmDialog()
                        hideKeyboard()
                    }

                    viewModel.terminateAddUserProfile()
                }
            }
        })

        viewModel.navigateToHomeFragment.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it){

                    val sp = requireActivity().getSharedPreferences(getString(R.string.shared_pref_name), MODE_PRIVATE)
                    val editor = sp.edit()
                    editor.putBoolean(getString(R.string.first_time_shp), false)
                    editor.commit()
                    findNavController().navigate(FirstTimeUserProfileFragmentDirections.actionFirstTimeUserProfileFragmentToHomeFragment())
                    viewModel.terminateNavigateToHomeFragment()
                }
            }
        })

        return binding.root
    }

    private fun welcomeDialog() {
        val ok = { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        val exit = { dialog: DialogInterface, which: Int ->
            requireActivity().finish()
        }

        MaterialAlertDialogBuilder(requireContext())
                .setIcon(R.drawable.ic_heart_red)
                .setCancelable(false)
                .setTitle("Welcome to BP Tracker!")
                .setMessage("Please fill the following fields accurately!\nNote: You can not start using the app WITHOUT filling this form!")
                .setPositiveButton("Ok", DialogInterface.OnClickListener(function = ok))
                .setNegativeButton("Exit", DialogInterface.OnClickListener(function = exit))
                .show()
    }

    private fun confirmDialog() {
        val now = { dialog: DialogInterface, which: Int ->
            fireNotificationSchedule()
            viewModel.initiateNavigateToHomeFragment()
            dialog.dismiss()
        }

        MaterialAlertDialogBuilder(requireContext())
                .setIcon(R.drawable.ic_heart_red)
                .setCancelable(false)
                .setTitle("User Profile added successfully!")
                .setMessage("As a friendly reminder, You will be reminded to take your readings at 12PM, 3 PM, and 9 PM")
                .setPositiveButton("Cool!", DialogInterface.OnClickListener(function = now))
                .show()
    }

    private fun hideKeyboard(){
        val view = activity?.currentFocus
        if(view != null){
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun fireNotificationSchedule() {


        val repeatingRequest
                = PeriodicWorkRequestBuilder<FireNotificationWorker>(20, TimeUnit.MINUTES)
            .build()


        WorkManager.getInstance(requireContext()).enqueueUniquePeriodicWork(
            FireNotificationWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.REPLACE,
            repeatingRequest
        )
    }
}