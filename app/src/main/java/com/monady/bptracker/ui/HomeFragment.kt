package com.monady.bptracker.ui

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.monady.bptracker.R
import com.monady.bptracker.databinding.FragmentHomeBinding
import com.monady.bptracker.models.view.UserProfileView
import com.monady.bptracker.viewmodel.UserProfileViewModel
import com.monady.bptracker.viewmodel.ViewModelFactory

class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    lateinit var viewModel: UserProfileViewModel
   override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
       (requireActivity() as AppCompatActivity).supportActionBar?.show()
       binding = FragmentHomeBinding.inflate(layoutInflater)

       /*val app = requireNotNull(this.activity).application

       val viewModelFactory = ViewModelFactory(app)

       viewModel = ViewModelProvider(this, viewModelFactory).get(UserProfileViewModel::class.java)

       binding.lifecycleOwner = this

       binding.toBpLists.setOnClickListener{
           findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToBpReadingsListFragment())
       }*/

       findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToBpReadingsListFragment())

        return binding.root
    }
}