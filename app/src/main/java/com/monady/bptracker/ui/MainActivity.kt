package com.monady.bptracker.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.monady.bptracker.R
import com.monady.bptracker.utils.NotificationsUtils

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        NotificationsUtils.createNotificationChannel(applicationContext)
        initFirstTimeUse()
    }

    private fun initFirstTimeUse(){
        val sharedPreferences = getSharedPreferences(getString(R.string.shared_pref_name), MODE_PRIVATE)

        if(!sharedPreferences.contains(getString(R.string.first_time_shp))){


            NotificationsUtils.buildNotification(applicationContext, "Welcome to BPTracker", "Wish you the best!")

            val editor = sharedPreferences.edit()

            editor.putBoolean(getString(R.string.first_time_shp), true)

            editor.commit()
        }
    }
}