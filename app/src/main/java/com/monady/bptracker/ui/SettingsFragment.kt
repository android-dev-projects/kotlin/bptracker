package com.monady.bptracker.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import com.google.gson.Gson
import com.monady.bptracker.databinding.FragmentSettingsBinding
import com.monady.bptracker.viewmodel.ViewModelFactory
import com.monady.bptracker.work.FireNotificationWorker
import java.util.concurrent.TimeUnit


class SettingsFragment : Fragment() {
    lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(layoutInflater)



        return binding.root
    }

}