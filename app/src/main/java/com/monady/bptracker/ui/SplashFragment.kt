package com.monady.bptracker.ui

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.monady.bptracker.R
import com.monady.bptracker.databinding.FragmentSplashBinding

class SplashFragment : Fragment() {
    lateinit var binding: FragmentSplashBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        binding = FragmentSplashBinding.inflate(layoutInflater)

        val sharedPreferences = requireNotNull(activity).getSharedPreferences(getString(R.string.shared_pref_name), MODE_PRIVATE)

        var handler = Handler()

        var runnable = Runnable{
            if(sharedPreferences.contains(getString(R.string.first_time_shp)) &&
                sharedPreferences.getBoolean(getString(R.string.first_time_shp), false)){
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToFirstTimeUserProfileFragment())
            }
            else{
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
            }
        }

        handler.postDelayed(runnable, 1000)

        return binding.root
    }
}