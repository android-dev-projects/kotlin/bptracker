package com.monady.bptracker.ui.bpfragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.monady.bptracker.databinding.FragmentBpReadingAddBinding
import com.monady.bptracker.models.view.BpReadingView
import com.monady.bptracker.viewmodel.BpViewModel
import com.monady.bptracker.viewmodel.ViewModelFactory

class BpReadingAddFragment : Fragment() {
    lateinit var binding: FragmentBpReadingAddBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBpReadingAddBinding.inflate(layoutInflater)

        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        val viewModel = ViewModelProvider(this, viewModelFactory).get(BpViewModel::class.java)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.bpReadingView = BpReadingView(0, 0L, 0L, "", "", 1)

        viewModel.addNewReading.observe(viewLifecycleOwner, Observer{
            it?.let {
                if (it) {
                    val reading = binding.bpReadingView!!
                    if (reading.high == "" || reading.low == "") {
                        Snackbar.make(
                            binding.root,
                            "One or more fields are set to empty!",
                            Snackbar.LENGTH_LONG
                        ).show()
                        viewModel.endAddNewReading()
                    }else{
                        reading.created_at = System.currentTimeMillis()
                        reading.updated_at = System.currentTimeMillis()
                        viewModel.insertReading(reading)
                        viewModel.beginNavigateToListReadings()
                        viewModel.endAddNewReading()
                        hideKeyboard()
                    }
                }
            }
        })

        viewModel.navigateToListReadings.observe(viewLifecycleOwner, Observer{
            it?.let{
                if(it){
                    findNavController().navigate(BpReadingAddFragmentDirections.actionBpReadingAddFragmentToBpReadingsListFragment())
                    viewModel.endNavigateToListReadings()
                }
            }
        })

        return binding.root
    }

    private fun hideKeyboard(){
        val view = activity?.currentFocus
        if(view != null){
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}