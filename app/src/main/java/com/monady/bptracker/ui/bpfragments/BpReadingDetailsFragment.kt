package com.monady.bptracker.ui.bpfragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.monady.bptracker.databinding.FragmentBpReadingDetailsBinding
import com.monady.bptracker.viewmodel.BpViewModel
import com.monady.bptracker.viewmodel.ViewModelFactory

class BpReadingDetailsFragment : Fragment() {
    lateinit var binding: FragmentBpReadingDetailsBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBpReadingDetailsBinding.inflate(layoutInflater)

        val arguments = BpReadingDetailsFragmentArgs.fromBundle(requireArguments())

        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        val viewModel = ViewModelProvider(this, viewModelFactory).get(BpViewModel::class.java)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        viewModel.getReadingById(arguments.readingId)

        viewModel.readingDetails.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.bpReadingView = it
            }
        })

        viewModel.deleteBpReadingAction.observe(viewLifecycleOwner, Observer{
            it?.let {
                if(it){
                    viewModel.deleteReading(binding.bpReadingView!!.id)
                    findNavController().navigate(BpReadingDetailsFragmentDirections.actionBpReadingDetailsFragmentToBpReadingsListFragment())
                    viewModel.terminateDeleteBpReadingAction()
                    Snackbar.make(binding.root, "Reading Deleted Successfully!", Snackbar.LENGTH_LONG).show()
                }
            }
        })

        viewModel.editUpdateButton.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.viewState = it
                if(!it){
                    viewModel.terminateUpdateBpReadingAction()
                }
            }
        })

        viewModel.updateBpReadingAction.observe(viewLifecycleOwner, Observer {
            it?.let{
                if(it){
                    binding.bpReadingView!!.updated_at = System.currentTimeMillis()
                    viewModel.updateReading(binding.bpReadingView!!)
                    viewModel.getReadingById(arguments.readingId)
                    viewModel.toggleEditUpdateButtonOff()
                    hideKeyboard()
                }
            }
        })

        return binding.root
    }

    private fun hideKeyboard(){
        val view = activity?.currentFocus
        if(view != null){
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}