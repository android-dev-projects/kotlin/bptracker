package com.monady.bptracker.ui.bpfragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.monady.bptracker.R
import com.monady.bptracker.adapters.BpReadingsAdapter
import com.monady.bptracker.databinding.FragmentBpReadingsListBinding
import com.monady.bptracker.listeners.BpReadingListener
import com.monady.bptracker.viewmodel.BpViewModel
import com.monady.bptracker.viewmodel.ViewModelFactory

class BpReadingsListFragment : Fragment() {
    lateinit var binding: FragmentBpReadingsListBinding
    lateinit var viewModel: BpViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBpReadingsListBinding.inflate(layoutInflater)

        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        viewModel = ViewModelProvider(this, viewModelFactory).get(BpViewModel::class.java)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.bpRecyclerview.adapter = BpReadingsAdapter(BpReadingListener { bpReadingId ->
            viewModel.beginNavigateToBpReadingDetails(bpReadingId)
        })

        viewModel.bpReadingsList.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.viewState = it.isEmpty() == true
            }
        })

        viewModel.navigateToBpReadingDetails.observe(viewLifecycleOwner, Observer {
            it?.let {
                findNavController().navigate(
                    BpReadingsListFragmentDirections.actionBpReadingsListFragmentToBpReadingDetailsFragment(it)
                )
                viewModel.endNavigateToBpReadingDetails()
            }
        })

        viewModel.navigateToAddNewReading.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it){
                    findNavController().navigate(BpReadingsListFragmentDirections.actionBpReadingsListFragmentToBpReadingAddFragment())
                    viewModel.endNavigateToAddNewReading()
                }
            }
        })

        viewModel.deleteAllEntriesAction.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    if(viewModel.bpReadingsList.value?.isEmpty() == true){
                        Snackbar.make(binding.root, "The list is already empty!", Snackbar.LENGTH_LONG).show()
                    }
                    else{
                        viewModel.deleteAllReadings()
                        Snackbar.make(binding.root, "All entries deleted successfully!", Snackbar.LENGTH_LONG).show()
                        viewModel.terminateDeleteAllEntriesAction()
                    }
                }
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_readings_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menu_delete_readings_list -> {
                viewModel.initiateDeleteAllEntriesAction()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}