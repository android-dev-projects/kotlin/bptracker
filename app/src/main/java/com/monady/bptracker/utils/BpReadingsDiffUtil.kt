package com.monady.bptracker.utils

import androidx.recyclerview.widget.DiffUtil
import com.monady.bptracker.models.domain.BpReading

class BpReadingsDiffUtil: DiffUtil.ItemCallback<BpReading>() {
    override fun areItemsTheSame(oldItem: BpReading, newItem: BpReading): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: BpReading, newItem: BpReading): Boolean {
        return oldItem == newItem
    }
}