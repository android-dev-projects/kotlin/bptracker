package com.monady.bptracker.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.monady.bptracker.R

class NotificationsUtils {
    companion object{
        private const val CHANNEL_ID = "100001"
        private var NOTIFICATION_ID = 0

        fun createNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = "Channel name"
                val description = "getString(R.string.channel_description)"
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(CHANNEL_ID, name, importance)
                channel.description = description
                val notificationManager = context.getSystemService(NotificationManager::class.java)
                notificationManager.createNotificationChannel(channel);
            }
        }

        fun buildNotification(context: Context, title: String, content: String ){
            var builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_heart_red)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            var notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(NOTIFICATION_ID++, builder.build());
        }
    }


}