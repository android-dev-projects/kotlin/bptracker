package com.monady.bptracker.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.monady.bptracker.models.domain.BpReading
import com.monady.bptracker.models.view.toDomainModel
import com.monady.bptracker.models.view.BpReadingView
import com.monady.bptracker.models.domain.toViewModel
import com.monady.bptracker.repository.DataRepository
import kotlinx.coroutines.*

class BpViewModel(private val app: Application): AndroidViewModel(app) {

    private val repo = DataRepository.getDataRepository(app)

    private val job = Job()

    private val scope = CoroutineScope(job + Dispatchers.IO)

    val bpReadingsList = liveData{
        emitSource(repo.getAllReadings())
    }

    private val _readingDetails = MutableLiveData<BpReadingView>()
    val readingDetails: LiveData<BpReadingView>
        get() = _readingDetails

    fun insertReading(reading: BpReadingView){
        scope.launch {
            repo.insertReading(reading.toDomainModel())
        }
    }

    fun getReadingById(readingId: Int){
        scope.launch {
            withContext(Dispatchers.IO){
                _readingDetails.postValue(repo.getReadingById(readingId))
            }
        }
    }

    fun updateReading(reading: BpReadingView){
        scope.launch {
            repo.updateReading(reading.toDomainModel())
        }
    }

    fun deleteReading(readingId: Int){
        scope.launch {
            repo.deleteReadingById(readingId)
        }
    }

    fun deleteAllReadings(){
        scope.launch {
            repo.deleteAllReadings()
        }
    }

    private val _navigateToBpReadingDetails = MutableLiveData<Int>()
    val navigateToBpReadingDetails: LiveData<Int>
        get() = _navigateToBpReadingDetails

    fun beginNavigateToBpReadingDetails(readingId: Int){
        _navigateToBpReadingDetails.value = readingId
    }
    fun endNavigateToBpReadingDetails(){
        _navigateToBpReadingDetails.value = null
    }

    private val _navigateToAddNewReading = MutableLiveData<Boolean>()
    val navigateToAddNewReading: LiveData<Boolean>
        get() = _navigateToAddNewReading

    fun beginNavigateToAddNewReading(){
        _navigateToAddNewReading.value = true
    }
    fun endNavigateToAddNewReading(){
        _navigateToAddNewReading.value = false
    }

    private val _addNewReading = MutableLiveData<Boolean>()
    val addNewReading: LiveData<Boolean>
        get() = _addNewReading

    fun beginAddNewReading(){
        _addNewReading.value = true
    }
    fun endAddNewReading(){
        _addNewReading.value = false
    }

    private val _navigateToListReadings = MutableLiveData<Boolean>()
    val navigateToListReadings: LiveData<Boolean>
        get() = _navigateToListReadings

    fun beginNavigateToListReadings(){
        _navigateToListReadings.value = true
    }
    fun endNavigateToListReadings(){
        _navigateToListReadings.value = false
    }

    private val _deleteBpReadingAction = MutableLiveData<Boolean>()
    val deleteBpReadingAction: LiveData<Boolean>
        get() = _deleteBpReadingAction

    fun initiateDeleteBpReadingAction(){
        _deleteBpReadingAction.value = true
    }
    fun terminateDeleteBpReadingAction(){
        _deleteBpReadingAction.value = false
    }

    private val _editUpdateButton = MutableLiveData<Boolean>()
    val editUpdateButton: LiveData<Boolean>
        get() = _editUpdateButton

    fun toggleEditUpdateButtonOn(){
        _editUpdateButton.value = true
    }
    fun toggleEditUpdateButtonOff(){
        _editUpdateButton.value = false
    }

    private val _updateBpReadingAction = MutableLiveData<Boolean>()
    val updateBpReadingAction: LiveData<Boolean>
        get() = _updateBpReadingAction

    fun initiateUpdateBpReadingAction(){
        _updateBpReadingAction.value = true
    }
    fun terminateUpdateBpReadingAction(){
        _updateBpReadingAction.value = false
    }

    private val _deleteAllEntriesAction = MutableLiveData<Boolean>()
    val deleteAllEntriesAction: LiveData<Boolean>
        get() = _deleteAllEntriesAction

    fun initiateDeleteAllEntriesAction(){
        _deleteAllEntriesAction.value = true
    }
    fun terminateDeleteAllEntriesAction(){
        _deleteAllEntriesAction.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}