package com.monady.bptracker.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.monady.bptracker.models.domain.toViewModel
import com.monady.bptracker.models.view.UserProfileView
import com.monady.bptracker.models.view.toDomainModel
import com.monady.bptracker.repository.DataRepository
import kotlinx.coroutines.*

class UserProfileViewModel(app: Application): AndroidViewModel(app) {

    private val repo = DataRepository.getDataRepository(app)

    private val job = Job()

    private val scope = CoroutineScope(job + Dispatchers.IO)

    val userProfile = Transformations.map(repo.getUserProfile()){
        liveData<UserProfileView>{
            emit(it.toViewModel())
        }
    }

    fun insertUserProfile(userProfileView: UserProfileView){
        scope.launch {
            repo.insertUserProfile(userProfileView.toDomainModel())
        }
    }

    fun updateUserProfile(userProfileView: UserProfileView){
        scope.launch {
            repo.updateUserProfile(userProfileView.toDomainModel())
        }
    }

    private val _addUserProfile = MutableLiveData<Boolean>()
    val addUserProfile: LiveData<Boolean>
        get() = _addUserProfile

    fun initiateAddUserProfile(){
        _addUserProfile.value = true
    }
    fun terminateAddUserProfile(){
        _addUserProfile.value = false
    }

    private val _navigateToHomeFragment = MutableLiveData<Boolean>()
    val navigateToHomeFragment: LiveData<Boolean>
        get() = _navigateToHomeFragment

    fun initiateNavigateToHomeFragment(){
        _navigateToHomeFragment.value = true
    }
    fun terminateNavigateToHomeFragment(){
        _navigateToHomeFragment.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

}