package com.monady.bptracker.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(val app: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BpViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BpViewModel(app) as T
        }
        if (modelClass.isAssignableFrom(UserProfileViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserProfileViewModel(app) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}