package com.monady.bptracker.work

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.monady.bptracker.utils.NotificationsUtils
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class FireNotificationWorker(context: Context, params: WorkerParameters): CoroutineWorker(
    context,
    params
){

    companion object{
        const val WORK_NAME =  "FireReminderNotifications"
        var x = 0
    }

    override suspend fun doWork(): Result {

        return try {
            val format = SimpleDateFormat("HH:mm")
            val time = format.format(Date()).split(":")

            val hours = time[0].toInt()
            val minutes = time[1].toInt()

            if(hours == 12 || hours == 15 || hours == 21)
            {
                Log.d("xTAG", "onCreate: $hours")
                Log.d("xTAG", "onCreate: $minutes")
                Log.d("xTAG", "doWork: Run ${x++}")
                if( hours > 12){
                    NotificationsUtils.buildNotification(applicationContext, "Its ${hours - 12}PM Reading Time!","")
                }
                else{
                    NotificationsUtils.buildNotification(applicationContext, "Its ${hours}PM Reading Time!","")
                }
            }
            return Result.success()
        }
        catch(e: Exception){
            return Result.retry()
        }
    }
}